package ictgradschool.industry.lab16.ex02.model;


import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener {

    private Course course;

    public CourseAdapter(Course course) {
        this.course = course;
    }

    @Override
    public void courseHasChanged(Course course) {
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return course.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        System.out.println(course.size());
        StudentResult studentResult = course.getResultAt(rowIndex);
        System.out.println(studentResult.toString());
        switch (columnIndex) {
            case 0:
                return studentResult._studentID;
            case 1:
                return studentResult._studentSurname;
            case 2:
                return studentResult._studentForename;
            case 3:
                return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Exam);
            case 4:
                return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Test);
            case 5:
                return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
            case 6:
                return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Overall);
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "student ID";

            case 1:
                return "Surname";

            case 2:
                return "Forename";

            case 3:
                return "Exam";

            case 4:
                return "Test";

            case 5:
                return "Assignment";

            case 6:
                return "Overall";

        }
        return null;
    }

}