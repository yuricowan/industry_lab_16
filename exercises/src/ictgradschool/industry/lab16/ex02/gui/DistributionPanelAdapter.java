package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

public class DistributionPanelAdapter implements CourseListener {

	/**********************************************************************
	 * YOUR CODE HERE
	 */
	protected Course course;
	protected DistributionPanel distributionPanel;

	public DistributionPanelAdapter(DistributionPanel distributionPanel, Course course) {
		this.distributionPanel = distributionPanel;
		this.course = course;
		course.addCourseListener(this);
	}


	@Override
	public void courseHasChanged(Course course) {
		distributionPanel.repaint();
	}
}
